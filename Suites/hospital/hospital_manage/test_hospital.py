import allure
import pytest

from base.selecet import select1
from page_action.hospital_action.hospital_manage_action.doctor_manage_action import doctor_manage_action
from page_action.hospital_action.hospital_manage_action.hospital_resource_action import hospital_resource_action
from page_action.hospital_action.scheduling_manage.scheduling_action import scheduling
from text_object.host_url import host_url



@allure.epic("机构运营管理平台")
@allure.feature("机构管理模块")
@allure.story("机构资源管理")
class Test_hospital(select1):
    '''

    '''
    # 获取添加医生的测试用例的ymal数据文件
    file1 = host_url().add_doctor_url()
    # 获取医生排班的测试用例的ymal数据文件
    file2 = host_url().scheduling_url()

    @allure.epic("机构运营管理平台")
    @allure.feature("机构管理模块")
    @allure.story("机构资源管理")
    @allure.title("添加医生功能")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseinfo",file1)
    @pytest.mark.flaky(reruns=1, reruns_delay=1)
    def test_adddoctor(self,caseinfo):
        '''
        主要测试新建医生的功能
        :return:
        '''
        hospital_resource_action().add_doctor(caseinfo['hospital_name'],host_url().idcard1(),caseinfo['doctor_name']
                                              ,host_url().ran_phone(),caseinfo['kind'],caseinfo['doctor_kind1'],
                                              caseinfo['doctor_kind2'],host_url().ran_phone())
        # # 删除医生信息的页面动作
        # hospital_resource_action().delete_doctor(caseinfo['hospital_name'])
        # # 取消医生签约信息的页面动作
        # doctor_manage_action().doctor_delete(caseinfo['hospital_name'])

    @allure.epic("机构运营管理平台")
    @allure.feature("机构管理模块")
    @allure.story("机构资源管理")
    @allure.title("医生排班功能")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize("caseone", file1)
    @pytest.mark.parametrize("casetwo",file2)
    @pytest.mark.flaky(reruns=1 ,reruns_delay=1)
    def test_scheduling_process(self,caseone,casetwo):
        '''
        主要测试后台医生排班的功能
        :param caseinfo:
        '''
        # 新增医生的页面动作
        hospital_resource_action().add_doctor(caseone['hospital_name'], host_url().idcard1(), caseone['doctor_name']
                                              , host_url().ran_phone(), caseone['kind'], caseone['doctor_kind1'],
                                              caseone['doctor_kind2'], host_url().ran_phone())
        # 医生服务开启的页面动作
        doctor_manage_action().doctor_service(caseone['hospital_name'])
        # 医生排班的页面动作
        scheduling().scheduling_process(caseone['hospital_name'], casetwo['hospital_choice'], casetwo['service_name']
                                        , caseone['kind'], caseone['doctor_name'], casetwo['cost_money'])
        # 删除医生信息的页面动作
        hospital_resource_action().delete_doctor(caseone['hospital_name'])
        # 取消医生签约信息的页面动作
        doctor_manage_action().doctor_delete(caseone['hospital_name'])

    @allure.epic("机构运营管理平台")
    @allure.feature("机构管理模块")
    @allure.story("机构资源管理")
    @allure.title("删除医生功能")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.flaky(reruns=1,reruns_delay=1)
    @pytest.mark.parametrize("caseone",file1)
    def test_deletedoctor(self,caseone):
        '''
        主要测试后台医生的数据的删除功能
        :return:
        '''
        # 删除医生信息的页面动作
        hospital_resource_action().delete_doctor(caseone['hospital_name'])

    @allure.epic("机构运营管理平台")
    @allure.feature("机构管理模块")
    @allure.story("机构资源管理")
    @allure.title("解除医生签约功能")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.flaky(reruns=1, reruns_delay=1)
    @pytest.mark.parametrize("caseone", file1)
    def test_delete_doctorservice(self, caseone):
        '''
        主要测试后台医生的签约信息的删除功能
        :return:
        '''
        # 取消医生签约信息的页面动作
        doctor_manage_action().doctor_delete(caseone['hospital_name'])
