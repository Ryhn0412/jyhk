import time

import allure
import pytest
from base.selecet import select1
from page_action.shopping_action.image_action.image_manage import sc_img
from text_object.host_url import host_url


@allure.epic("商城后台管理系统")
@allure.feature("图片管理模块")
@allure.severity(allure.severity_level.BLOCKER)
class Test_sc_img(select1):
    '''
    关于图片库的测试用例
    '''

    # # 新增图片信息的ymal文件
    # file1 = host_url().addimage_url()
    # # 查找图片信息的ymal文件
    # file2 = host_url().selectimage_url()



    @allure.epic("商城后台管理系统")
    @allure.feature("店铺模块")
    @allure.story("图片管理")
    @allure.title("增加图片1")
    @pytest.mark.flaky(reruns=1, reruns_delay=2)
    @allure.severity(allure.severity_level.CRITICAL)
    def test_addimg1(self):
        '''
        负责添加图片的功能测试用例1
        :return:
        '''
        sc_img().addimg1()

    @allure.epic("商城后台管理系统")
    @allure.feature("店铺模块")
    @allure.story("图片管理")
    @allure.title("增加图片2")
    @pytest.mark.flaky(reruns=1, reruns_delay=2)
    @allure.severity(allure.severity_level.CRITICAL)
    def test_addimg2(self):
        '''
        负责添加图片的功能测试用例2
        :return:
        '''
        sc_img().addimg2()
        sc_img().deleteimg()

    @allure.epic("商城后台管理系统")
    @allure.feature("店铺模块")
    @allure.story("图片管理")
    @allure.title("增加图片3")
    @pytest.mark.flaky(reruns=1, reruns_delay=2)
    @allure.severity(allure.severity_level.CRITICAL)
    def test_addimg3(self):
        '''
        负责添加图片的功能测试用例3
        :return:
        '''
        sc_img().addimg3()
        sc_img().deleteimg()

    @allure.epic("商城后台管理系统")
    @allure.feature("店铺模块")
    @allure.story("图片管理")
    @allure.title("查找图片")
    @allure.severity(allure.severity_level.CRITICAL)
    # @pytest.mark.parametrize("caseone", file1)#
    # @pytest.mark.parametrize("casetwo", file2)
    @pytest.mark.flaky(reruns=1, reruns_delay=2)
    def test_selectimg(self):
        '''
        负责查找图片的功能测试用例
        '''
        sc_img().selectimg()

    @allure.severity(allure.severity_level.CRITICAL)
    @allure.epic("商城后台管理系统")
    @allure.feature("店铺模块")
    @allure.story("图片管理")
    @allure.title("删除图片")
    def test_deleteimg(self):
        '''
        负责删除图片的功能测试用例
        :return:
        '''
        sc_img().deleteimg()