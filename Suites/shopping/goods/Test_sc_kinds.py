import time

import allure
import pytest
from selenium.webdriver import ActionChains

from base.selecet import select1
from page_action.shopping_action.goods_action.goods import goods


@allure.epic("商城后台管理系统")
@allure.feature("商品管理模块")
@allure.story("商品管理")
class Test_sc_kinds(select1):
    '''
    关于商品管理的页面动作
    '''


    @pytest.mark.flaky(reruns=1, reruns_delay=2)
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.epic("商城后台管理系统")
    @allure.feature("商品管理模块")
    @allure.story("商品管理")
    @allure.title("添加商品分类")
    def test_addkinds(self):
        """
        主要用于测试添加商品分类功能
        """
        goods().addkinds()


    @pytest.mark.flaky(reruns=1, reruns_delay=2)
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.epic("商城后台管理系统")
    @allure.feature("商品管理模块")
    @allure.story("商品管理")
    @allure.title("删除商品分类")
    def test_deletekinds(self):
        """
        用于删除商品分类功能的用例
        """
        goods().deletekinds()