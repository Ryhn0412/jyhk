import time
import allure
import pytest
from numpy.distutils.log import good

from base.selecet import select1
from page_action.shopping_action.goods_action.goods import goods
from page_action.shopping_action.image_action.image_manage import sc_img


@allure.epic("商城后台管理系统")
@allure.feature("商品管理模块")
@allure.story("商品管理")
@allure.severity(allure.severity_level.BLOCKER)
class Test_shangchen(select1):
    '''
    商品管理模块的测试用例
    '''

    @allure.epic("商城后台管理系统")
    @allure.feature("商品管理模块")
    @allure.story("商品管理")
    @allure.title("添加商品")
    @pytest.mark.flaky(reruns=1, reruns_delay=2)
    @allure.severity(allure.severity_level.CRITICAL)
    def test_addgoods(self):
        """
        主要用于测试添加商品的功能测试
        """
        goods().addgoods()
        time.sleep(1)
        sc_img().deleteimg()


    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.flaky(reruns=1, reruns_delay=2)
    @allure.epic("商城后台管理系统")
    @allure.feature("商品管理模块")
    @allure.story("商品管理")
    @allure.title("查找商品")
    def test_selectgoods(self):
        """
        主要用于测试查找流程的功能测试用例
        """
        goods().selectgoods()


    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.flaky(reruns=1, reruns_delay=2)
    @allure.epic("商城后台管理系统")
    @allure.feature("商品管理模块")
    @allure.story("商品管理")
    @allure.title("删除商品")
    def test_deletegoods(self):
        """
        主要用于测试删除商品功能
        """
        goods().deletegoods()








