import datetime
import time

import allure
import pytest

from base.selecet import select1
from page_action.shopping_action.order_action.order_manage import order
from text_object.host_url import host_url


@allure.epic("商城后台管理系统")
@allure.feature("订单管理模块")
@allure.severity(allure.severity_level.BLOCKER)
class Test_sc_buys(select1):
    '''
    商城管理系统的订单模块
    '''

    # 查询订单所用到的ymal文件
    file1 = host_url().order_select_url()


    @allure.epic("商城后台管理系统")
    @allure.feature("订单管理模块")
    @allure.story("订单管理")
    @allure.title("查询功能")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.flaky(reruns=1, reruns_delay=1)
    @pytest.mark.parametrize("caseone",file1)
    def test_selectbuys(self,caseone):
        """
        根据搜索条件进行查询
        """
        order().select_buys1(caseone['select_name'],
                             caseone['buys_name'], caseone['start_time'])

    @allure.epic("商城后台管理系统")
    @allure.feature("订单管理模块")
    @allure.story("订单管理")
    @allure.title("订单状态界面展示1")
    @allure.severity(allure.severity_level.NORMAL)
    @pytest.mark.flaky(reruns=1, reruns_delay=1)
    def test_selectbuys1(self):
        '''
        进行订单界面展示（待付款）
        '''
        order().buff_look1()

    @allure.epic("商城后台管理系统")
    @allure.feature("订单管理模块")
    @allure.story("订单管理")
    @allure.title("订单状态界面展示2")
    @allure.severity(allure.severity_level.NORMAL)
    @pytest.mark.flaky(reruns=1, reruns_delay=1)
    def test_selectbuys2(self):
        '''
        进行订单界面展示
        '''
        order().buff_look2()

    @allure.epic("商城后台管理系统")
    @allure.feature("订单管理模块")
    @allure.story("订单管理")
    @allure.title("订单状态界面展示3")
    @allure.severity(allure.severity_level.NORMAL)
    @pytest.mark.flaky(reruns=1, reruns_delay=5)
    def test_selectbuys3(self):
        '''
        进行订单界面展示（待配送）
        '''
        order().buff_look3()
