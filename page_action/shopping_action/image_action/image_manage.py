import time

import allure
import pytest
from selenium.webdriver.common.by import By

from base.selecet import select1
from tools.yaml_decode import image_locator


@allure.epic("商城后台管理系统")
@allure.feature("图片管理模块")
@allure.severity(allure.severity_level.BLOCKER)
class sc_img(select1):
    '''
    关于图片模块的页面动作
    '''

    def set_up(self):
        '''
        初始化浏览器
        :return:
        '''
        with allure.step("初始化浏览器"):
            self.shangcheng()
            self.driver.implicitly_wait(5)
            self.url('https://pre-release.jyhk.com/mall/#/shopmall/pics')
            time.sleep(1)

    def quit_image(self):
        '''
        浏览器退出
        :return:
        '''
        with allure.step("退出浏览器"):
            time.sleep(1)
            self.catch_png()
            time.sleep(2)
            self.quit()
            print("_______________________测试结束____________________________")

    # 确定删除按钮
    yep_button = '//div[@class="el-message-box__wrapper"]/div[1]/div[3]/button[2]'
    # 页面数据显示
    yemian_text = '//*[@id="app"]/div/div[2]/section/div/div[4]/div/span[1]'
    # 新建商品的删除按钮
    newbuys_deletebutton = '//*[@id="app"]/div/div[2]/section/div/div[3]/div[3]/table/tbody/tr[last()]/td[last()]/div/button[2]/span'
    # 重置按钮
    return_button = '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[3]/div/button[2]/span'
    # 查询按钮
    select2_button = '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[3]/div/button[1]'
    # 搜索图片分类框
    kinds_select = '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[2]/div/div/div'
    # 搜索输入框
    select_text = '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[1]/div/div/input'
    # 新增商品按钮
    add_button = '//*[@id="app"]/div/div[2]/section/div/div[2]/div/button[1]'
    # 分类框按钮
    kinds_button = '//div[@class="el-select el-input-medium el-select--small"]/div[1]/input'
    # 图片上传按钮
    image_button = '//div[@class="el-form-item relative is-required"]/div[1]/div[1]/div[1]/div[1]/input'
    # 提交按钮
    upload_button = '//*[@class="el-dialog__footer"]/div/div/div/button[2]'
    # 确认提交按钮
    button1 = '//*[@id="app"]/div/div[2]/section/div/div[1]/div/div[3]/div/button[1]'
    # 头像分类按钮
    image_button1 = '//div[@class="el-select-dropdown el-popper" and @x-placement="bottom-start"]/div[1]/div[1]/ul[1]/li[1]/span'
    # 商品分类按钮
    buys_button1 = '//div[@class="el-select-dropdown el-popper" and @x-placement="bottom-start"]/div[1]/div[1]/ul[1]/li[2]/span'
    # 横幅分类按钮
    hengfu_button1 = '//div[@class="el-select-dropdown el-popper" and @x-placement="bottom-start"]/div[1]/div[1]/ul[1]/li[3]/span'

    def addimg1(self):
        '''
        负责添加图片的页面动作1
        :return:
        '''
        self.set_up()
        with allure.step('点击新增商品按钮'):
           self.click(self.add_button)
        with allure.step('选择图片分类'):
           self.click(self.kinds_button)
           self.click(self.image_button1)
        with allure.step('上传图片'):
           self.input(self.image_button,image_locator(r'\ocr.png'))
           time.sleep(2)
        with allure.step('点击提交'):
            time.sleep(2)
            self.click(self.upload_button)
            self.click(self.button1)
        time.sleep(1)
        self.quit_image()

    def addimg2(self):
        '''
        负责添加图片的页面动作2
        :return:
        '''
        self.set_up()
        with allure.step('点击新增商品按钮'):
           self.click(self.add_button)
        with allure.step('选择图片分类'):
           self.click(self.kinds_button)
           self.click(self.buys_button1)
        with allure.step('上传图片'):
           self.input(self.image_button,image_locator(r'\ocr.png'))
           time.sleep(2)
        with allure.step('点击提交'):
            time.sleep(2)
            self.click(self.upload_button)
            self.click(self.button1)
        time.sleep(1)
        self.quit_image()

    def addimg3(self):
        '''
        负责添加图片的页面动作3
        :return:
        '''
        self.set_up()
        with allure.step('点击新增商品按钮'):
           self.click(self.add_button)
        with allure.step('选择图片分类'):
           self.click(self.kinds_button)
           self.click(self.hengfu_button1)
        with allure.step('上传图片'):
           self.input(self.image_button,image_locator(r'\ocr.png'))
           time.sleep(2)
        with allure.step('点击提交'):
            time.sleep(2)
            self.click(self.upload_button)
            self.click(self.button1)
        time.sleep(1)
        self.quit_image()




    def selectimg(self):
        '''
        负责查找图片的页面动作
        '''
        self.set_up()
        for i in range(1, 3):
            with allure.step('输入搜索名称'):
                self.input(self.select_text, 'ocr')
                time.sleep(1)
            with allure.step('选择搜索种类'):
                self.click(self.kinds_select)
                time.sleep(1)
            if i == 1:
                self.click(self.image_button1)
                time.sleep(1)
            elif i == 2:
                self.click(self.buys_button1)
                time.sleep(1)
            elif i == 3:
                self.click(self.hengfu_button1)
                time.sleep(1)
                with allure.step('点击查询'):
                    self.click(self.select2_button)
                    time.sleep(1)
                with allure.step('重置查询'):
                    self.click(self.return_button)
                    self.click(self.select2_button)
                    time.sleep(2)
                    break
            with allure.step('点击查询'):
                self.click(self.select2_button)
                time.sleep(1)
            with allure.step('重置查询'):
                self.click(self.return_button)
                self.click(self.select2_button)
                time.sleep(2)
        self.quit_image()


    def deleteimg(self):
        '''
        负责删除图片的功能测试用例
        :return:
        '''
        self.set_up()
        a = self.getText(self.yemian_text)
        time.sleep(1)
        with allure.step('点击删除'):
            self.click(self.newbuys_deletebutton)
            self.click(self.yep_button)
            time.sleep(2)
        with allure.step("审查是否删除"):
            b = self.getText(self.yemian_text)
            assert b != a
        self.quit_image()