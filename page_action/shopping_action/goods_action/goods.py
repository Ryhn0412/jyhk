import time

import allure
from selenium.webdriver import ActionChains

from base.selecet import select1
from tools.yaml_decode import image_locator


class goods(select1):
    '''
    有关于商品管理的页面动作
    '''

    # 平台分类选择框按钮
    kinds_select1 = '//*[@id="app"]/div/div[2]/section/form/div[1]/div[2]/div[3]/div/div/div/input'
    # 店内分类选择框按钮
    shopkinds_select1 = '//*[@id="app"]/div/div[2]/section/form/div[1]/div[2]/div[4]/div/div/div/input'
    # 售价框
    price_text = '//*[@id="app"]/div/div[2]/section/form/div[2]/div[2]/div[3]/div/div[1]/input'
    # 上架时间选择框
    time_select1 = '//*[@id="app"]/div/div[2]/section/form/div[2]/div[2]/div[7]/div/div/div/input'
    # 商品名称按钮选项
    buysname_button ='//div[@class="el-select-dropdown__wrap el-scrollbar__wrap"]/ul[1]/li[1]'
    # 商品id按钮选项
    buysid_button = '//div[@class="el-select-dropdown__wrap el-scrollbar__wrap"]/ul[1]/li[2]'
    def set_up(self):
        '''
        初始化浏览器
        :return:
        '''
        with allure.step("初始化浏览器"):
            self.shangcheng()
            self.driver.implicitly_wait(5)
            self.url('https://pre-release.jyhk.com/mall/#/goods/goodList')
            time.sleep(1)

    def quit_goods(self):
        '''
        浏览器退出
        :return:
        '''
        with allure.step("退出浏览器"):
            time.sleep(1)
            self.catch_png()
            time.sleep(2)
            self.quit()
            print("_______________________测试结束____________________________")


    def addkinds(self):
        """
        主要用于添加商品页面动作
        """
        self.set_up()
        with allure.step("点击新建按钮"):
            self.click('//*[@id="app"]/div/div[2]/section/div/div[4]/div[1]/div/div/button/span')
        with allure.step("输入新建分类名称"):
            self.click('/html/body/div[2]/div/div[2]/form/div/div/div[1]/div[1]/input')
            self.input('/html/body/div[2]/div/div[2]/form/div/div/div[1]/div[1]/input', '测试')
            self.click('/html/body/div[2]/div/div[3]/div/div/div[2]/button[2]/span')
        self.quit_goods()

    def deletekinds(self):
        """
        用于删除商品分类功能的用例
        """
        self.set_up()
        with allure.step("点击新建的分类切换按钮"):
            self.click('//*[@id="app"]/div/div[2]/section/div/div[4]/div[1]/div/ul/li[2]/div[1]')
        with allure.step("点击编辑按钮"):
            self.click('//*[@id="app"]/div/div[2]/section/div/div[4]/div[1]/div/ul/li[2]/span[2]')
        with allure.step("进行删除操作"):
            self.click('/html/body/div[2]/div/div[3]/div/div/div[1]/span/span/span/button')
            self.click('//*[@class="btn-group"]/button[2]')
        self.quit_goods()

    def addgoods(self):
        """
        主要用于添加商品的页面动作
        """
        self.set_up()
        with allure.step('点击新增商品'):
          self.click('//*[@id="app"]/div/div[2]/section/div/div[3]/div[1]/button[1]')
          time.sleep(2)
        with allure.step('输入相对应内容'):
          # 商品姓名
          self.input('//*[@id="app"]/div/div[2]/section/form/div[1]/div[2]/div[1]/div/div[1]/div[1]/input','测试')
          # 图片上传
          self.input('//*[@id="app"]/div/div[2]/section/form/div[1]/div[2]/div[2]/div/div/div[1]/div/input', image_locator(r'\ocr.png'))
          # 平台分类
          self.click(self.kinds_select1)
          self.tagclick('span','体检')
          # 分类选择
          self.click(self.shopkinds_select1)
          self.tagclick('span','测试分类1')
          # 商品售价
          self.click(self.price_text)
          self.input(self.price_text,'30')
          # 商品库存
          self.click('//*[@id="app"]/div/div[2]/section/form/div[2]/div[2]/div[5]/div/label/span[1]/span')
          # 上架选择
          self.click(self.time_select1)
          time.sleep(2)
          self.tagclick('span','立即上架')
          # 有效期
          self.tagclick('span','无有效期')
          self.tagclick('span','不限制使用时间')
          self.click('//*[@id="wangeditor"]/div[2]/div[1]')
          self.input('//*[@id="wangeditor"]/div[2]/div[1]','1111')
          self.click('//*[@id="app"]/div/div[2]/section/form/div[4]/button')
        self.quit_goods()

    def selectgoods(self):
        """
        主要用于查找流程的页面动作
        """
        self.set_up()
        with allure.step("根据商品名称查找"):
            element1 = self.locator('//*[@id="app"]/div/div[2]/section/div/div[2]/div/div[1]/div/div/div/div/div/input')
            ActionChains(self.driver).move_to_element(element1).click().perform()

            time.sleep(2)

            element2 = self.locator(self.buysname_button)
            ActionChains(self.driver).move_to_element(element2).click().perform()

            self.input('//*[@class="search-item"]/div/input', '保湿乳')

            element3 = self.locator('//*[@id="app"]/div/div[2]/section/div/div[2]/div/div[2]/div/button[1]')
            ActionChains(self.driver).move_to_element(element3).click().perform()
            time.sleep(2)
        with allure.step("重置功能检测"):
            self.click('//*[@id="app"]/div/div[2]/section/div/div[2]/div/div[2]/div/button[2]')
            self.click('//*[@id="app"]/div/div[2]/section/div/div[2]/div/div[2]/div/button[1]')
            time.sleep(2)
        with allure.step("根据商品ID查询"):
            ActionChains(self.driver).move_to_element(element1).click().perform()
            time.sleep(2)

            self.tagclick('span','商品ID')

            self.input('//*[@class="search-item"]/div/input', '14855528')
            ActionChains(self.driver).move_to_element(element3).click().perform()
        self.quit_goods()

    def deletegoods(self):
        """
        主要用于删除商品的页面动作
        """
        self.set_up()
        with allure.step("进行商品删除"):
            self.click(
                '//*[@id="app"]/div/div[2]/section/div/div[4]/div[2]/div/div[3]/table/tbody/tr[last()]/td[7]/div/button[3]/span')
            self.click('//*[@class="el-message-box"]/div[3]/button[2]')
        self.quit_goods()