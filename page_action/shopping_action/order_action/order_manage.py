import datetime
import time

import allure
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By

from base.selecet import select1


class order(select1):
    '''
    有关于订单管理模块的页面动作
    '''
    # 订单管理模块的url地址
    url1 = 'https://pre-release.jyhk.com/mall/#/ordermall/orderList'
    # 商品搜索选项栏按钮
    select_button = \
        '//div[@class="search-item-input input-with-select el-input el-input--small el-input-group el-input-group--prepend el-input--suffix"]' \
        '/div[1]/div[1]/div[1]/span[1]'
    # 搜索栏具体信息填写框
    select_text = '//*[@id="app"]/div/div[2]/section/div/div[2]/div/div[1]/div/div/input'
    # 开始时间输入框
    starttime_text = '//*[@id="app"]/div/div[2]/section/div/div[2]/div/div[2]/div/div/input[1]'
    # 结束时间输入框
    endtime_text = '//*[@id="app"]/div/div[2]/section/div/div[2]/div/div[2]/div/div/input[2]'
    # 查询按钮
    select1 = '//*[@id="app"]/div/div[2]/section/div/div[2]/div/div[3]/div/button[1]/span'
    # 重置查询按钮
    select2 = '//*[@id="app"]/div/div[2]/section/div/div[2]/div/div[3]/div/button[2]/span'

    def set_up(self):
        '''
        初始化浏览器
        :return:
        '''
        with allure.step("初始化浏览器"):
            self.shangcheng()
            self.url(self.url1)
            self.driver.implicitly_wait(10)

    def quit_order(self):
        '''
        退出浏览器
        :return:
        '''
        with allure.step("查询成功，关闭浏览器"):
            time.sleep(2)
            self.quit()
            print("------测试结束------")

    def select_buys1(self, select_name, buys_name, start_time):
        """
        进行查询的页面动作
        """
        self.set_up()
        with allure.step("点击搜索选项栏"):
            self.click(self.select_button)
            self.tagclick('span',select_name)
        with allure.step("输入相对应名称"):
            self.click(self.select_text)
            self.input(self.select_text,buys_name)
        with allure.step("输入创建时间"):
            self.click(self.starttime_text)
            time.sleep(1)
            self.click('//div[@class="el-picker-panel el-date-range-picker el-popper has-sidebar"]/div[1]/div[1]/button[1]')
            self.click(self.endtime_text)
            # 获取当前时间
            date1 = datetime.date.today().strftime('%Y-%m-%d')
            self.input(self.endtime_text,date1)
        with allure.step("开始查询"):
            self.click(self.select1)
            time.sleep(2)
        with allure.step("审查查询结果"):
            self.catch_png()
            time.sleep(1)
        with allure.step('重置查询内容'):
            self.click(self.select2)
            self.click(self.select1)
        self.quit_order()

    def buff_look1(self):
        '''
        待付款页面的查看动作
        :return:
        '''
        self.set_up()
        with allure.step("点击相对应状态的切换卡"):
            element1 = self.driver.find_element(By.ID,'tab-0')
            ActionChains(self.driver).move_to_element(element1).click().perform()
        with allure.step("截图实际情况"):
            time.sleep(1)
            self.catch_png()
            time.sleep(2)
        self.quit_order()

    def buff_look2(self):
        '''
        支付中页面的查看动作
        :return:
        '''
        self.set_up()
        with allure.step("点击相对应状态的切换卡"):
            element1 = self.driver.find_element(By.ID,'tab-1')
            ActionChains(self.driver).move_to_element(element1).click().perform()
        with allure.step("截图实际情况"):
            time.sleep(1)
            self.catch_png()
            time.sleep(2)
        self.quit_order()

    def buff_look3(self):
        '''
        待配送页面的查看动作
        :return:
        '''
        self.set_up()
        with allure.step("点击相对应状态的切换卡"):
            element1 = self.driver.find_element(By.ID,'tab-3')
            ActionChains(self.driver).move_to_element(element1).click().perform()
        with allure.step("截图实际情况"):
            time.sleep(1)
            self.catch_png()
            time.sleep(2)
        self.quit_order()

