import datetime
import time

import allure

from base.selecet import select1


class scheduling(select1):
    '''
    主要内容为排班管理的页面动作
    '''
    # 排班管理的地址
    scheduling_url = 'https://pre-release.jyhk.com/admin/#/scheduling/platform'
    # 排班日期输入框
    scheduling_datatext = '//form[@class="el-form"]/div[1]/div[1]/div[1]/div[1]/div[1]/input'
    # 门诊班次选择框
    outpatient_select = '//form[@class="el-form"]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/input'
    # 分院选择框
    hospital_select = '//form[@class="el-form"]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/input'
    # 服务类型选择框
    service_select = '//form[@class="el-form"]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/input'
    # 挂号科室选择框
    hospitalkind_select = '//form[@class="el-form"]/div[1]/div[5]/div[1]/div[1]/div[1]/div[1]/input'
    # 医生姓名选择
    doctor_select = '//form[@class="el-form"]/div[1]/div[6]/div[1]/div[1]/div[1]/div[1]/input'
    # 挂号费用
    cost_text = '//form[@class="el-form"]/div[1]/div[8]/div[1]/div[1]/div[1]/input'
    # 挂号数量调整
    many_text = '//div[@class="el-dialog__wrapper institutions-modal-wrapper"]/div[1]/div[2]/div[1]/div[1]/div[1]/p[1]/div[1]/div[1]/input'
    # 号源全选按钮
    button1 = '//div[@class="el-dialog__wrapper institutions-modal-wrapper"]' \
              '/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/table/thead/tr/th/div/label/span'
    # 预约的全选按钮
    button2 = '//div[@class="el-dialog__wrapper institutions-modal-wrapper"]' \
              '/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]/table/thead/tr/th/div[1]/label/span'
    # 保存预约途径按钮
    button3 = '//div[@class="el-dialog__wrapper institutions-modal-wrapper"]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/button[4]'
    # 保存并发布按钮
    button4 = '//div[@class="el-dialog__wrapper institutions-modal-wrapper"]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/span[1]/button[4]'
    # 页面数据显示
    text1 = '//*[@id="app"]/div/div[2]/section/div/div[2]/div[4]/div/div/span[1]'
    # 开始日期的查找输入框
    starttime_text = '//div[@class="z-container p_no z-branch-page"]/div[2]/div[1]/div[2]/div[3]/div/div/input[1]'
    # 结束日期的查找输入框
    endtime_text = '//div[@class="z-container p_no z-branch-page"]/div[2]/div[1]/div[2]/div[3]/div/div/input[2]'
    # 查找按钮
    select_button = '//section[@class="app-main"]/div/div[2]/div[1]/div[1]/div[4]/div/button[1]/span'
    # 审查姓名文本框
    doctorname_text = '//section[@class="app-main"]/div[1]/div[2]/div[3]/div[3]/table[1]/tbody[1]/tr[1]/td[4]/div[1]'
    def setup(self):
        '''
        初始化浏览器
        :return:
        '''
        with allure.step("初始化浏览器"):
            self.jigou()
            self.driver.implicitly_wait(5)
            self.url(self.scheduling_url)

    def quit_hospital(self):
        '''
        关闭浏览器
        :return:
        '''
        with allure.step('关闭浏览器'):
            self.catch_png()
            time.sleep(2)
            self.quit()
            print('_________________________该条测试用例结束_________________________')

    def scheduling_process(self, hospital_name, hospital_choice, service_name, kind_name, doctor_name, cost_money):
        '''
        主要为医生排班的流程
        :return:
        '''
        # 初始化
        self.setup()
        with allure.step("选择对应医院"):
            self.tagclick('li',hospital_name)
            time.sleep(1)
        with allure.step("进行查找"):
            # 开始时间的查找框输入
            self.click(self.starttime_text)
            data1 = datetime.datetime.now().strftime('%Y-%m-%d')
            time.sleep(1)
            self.input(self.starttime_text,data1)
            # 结束时间的查找框输入
            self.click(self.endtime_text)
            self.input(self.endtime_text,'2100-01-10')
            # 点击查找按钮
            self.click(self.select_button)
            time.sleep(2)
            a = self.getText(self.text1)
            time.sleep(1)
        with allure.step("点击新增排班按钮"):
            self.tagclick('span','新增')
        with allure.step("填写新增排班信息"):
            # 排班日期填写
            self.click(self.scheduling_datatext)
            self.input(self.scheduling_datatext,data1)
            # 门诊班次填写
            self.click(self.outpatient_select)
            self.tagclick('span','全天')
            # 分院选择填写
            self.click(self.hospital_select)
            self.tagclick('span',hospital_choice)
            # 服务类型填写
            self.click(self.service_select)
            self.tagclick('span',service_name)
            # 挂号科室填写
            self.click(self.hospitalkind_select)
            self.tagclick('span',kind_name)
            # 医生姓名填写
            self.click(self.doctor_select)
            self.tagclick('span',doctor_name)
            # 挂号费用填写
            self.click(self.cost_text)
            self.input(self.cost_text,cost_money)
            # 点击下一步
            self.tagclick('span','下一步')
        with allure.step("填写号源调整内容"):
            time.sleep(1)
            self.click(self.many_text)
            self.clear(self.many_text)
            self.input(self.many_text,'1')
            # 点击号源全选按钮
            self.click(self.button1)
            # 点击预约全选按钮
            self.click(self.button2)
            # 点击保存预约途径按钮
            self.click(self.button3)
            # 点击保存并发布按钮
            self.click(self.button4)
        with allure.step("审查排班是否成功"):
            time.sleep(2)
            b = self.getText(self.text1)
            time.sleep(1)
            assert b != a
        self.quit_hospital()