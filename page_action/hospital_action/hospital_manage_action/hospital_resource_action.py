import time

import allure
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By

from base.selecet import select1


class hospital_resource_action(select1):
    '''
    机构管理模块的机构资源管理的页面动作
    '''
    # 医生管理界面切换卡
    doctor_card = '//section[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]'
    # 机构资源管理页面的url
    hospital_resource_url = 'https://pre-release.jyhk.com/admin/#/organization/branchResource'
    # 新增医生的按钮
    add_doctor_button = '//*[@id="pane-2"]/div[1]/div[2]/div[1]/button[1]'
    # 证件号输入框
    idcard_text = '//form[@class="el-form el-form--label-right"]/div[1]/div[2]/div[1]/div[1]/div[1]/input'
    # 姓名输入框
    name_text = '//form[@class="el-form el-form--label-right"]/div[1]/div[3]/div[1]/div[1]/div[1]/input'
    # 手机号输入框
    phone_text = '//form[@class="el-form el-form--label-right"]/div[1]/div[7]/div[1]/div[1]/div[1]/input'
    # 所属区域选择框
    area_select = '//form[@class="el-form el-form--label-right"]/div[1]/div[11]/div[1]/div[1]/div[1]/div[1]/input'
    # 科室关联选择框
    kind_select = '//form[@class="el-form el-form--label-right"]/div[2]/div[1]/div[1]/div[1]/div[1]'
    # 医生职称一级选择框
    doctor_kind_select1 = '//form[@class="el-form el-form--label-right"]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/input'
    # 医生职称二级选择框
    doctor_kind_select2 = '//form[@class="el-form el-form--label-right"]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/input'
    # 主任医生按钮
    importdoctor_button = '//div[@class="el-select-dropdown el-popper" and @x-placement="bottom-start"]/div[1]/div[1]/ul[1]/li[3]/span'
    # 擅长疾病输入框
    sick_text = '//form[@class="el-form el-form--label-right"]/div[2]/div[3]/div[1]/div[1]/div[1]/input'
    # 保存按钮
    button1 = '//div[@aria-label="新增医生"]/div[3]/div[1]/div[1]/button[2]'
    # 工号输入框
    text_id = '//form[@class="el-form"]/div[1]/div[1]/div[1]/div[1]/div[1]/input'
    # 签约科室选择框
    kinds_select = '//form[@class="el-form"]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/input'
    # 信息页数显示
    yeshu_text = '//*[@id="pane-2"]/div/div[4]/div/span[1]'
    # 新建医生的删除按钮
    newdoctor_delete = '//div[@id="pane-2"]/div[1]/div[3]/div[4]/div[2]/table/tbody/tr[last()]/td[last()]/div/span[3]'
    # 确定删除按钮
    delete_button = '//div[@class="el-message-box"]/div[3]/button[2]'
    def setup(self):
        '''
        初始化浏览器
        :return:
        '''
        with allure.step("初始化浏览器"):
            self.jigou()
            self.driver.implicitly_wait(5)
            self.url(self.hospital_resource_url)

    def quit_hospital(self):
        '''
        关闭浏览器
        :return:
        '''
        with allure.step('关闭浏览器'):
            self.catch_png()
            time.sleep(2)
            self.quit()
            print('_________________________该条测试用例结束_________________________')

    def add_doctor(self,hospital_name,idcard12,doctor_name,phone1,kind,doctor_kind1,doctor_kind2,idd):
        '''
        添加相应医生至相应界面的页面动作
        :return:
        '''
        self.setup()
        with allure.step('选择相对应医院'):
            self.tagclick('li',hospital_name)
        with allure.step('切换至医生管理界面'):
            self.click(self.doctor_card)
            time.sleep(2)
            text1 = self.getText(self.yeshu_text)
            time.sleep(1)
        with allure.step('点击新建医生按钮'):
            self.click(self.add_doctor_button)
        with allure.step('填写新建医生信息'):
            # 输入证件号
            self.click(self.idcard_text)
            self.input(self.idcard_text,idcard12)
            # 输入姓名
            self.click(self.name_text)
            self.input(self.name_text,doctor_name)
            # 输入手机号
            time.sleep(1)
            self.click(self.phone_text)
            self.input(self.phone_text,phone1)
            # 所得区域选择框
            self.click(self.area_select)
            self.tagclick('span','北京市')
            self.tagclick('span','东城区')
            # 科室选择框
            self.click(self.kind_select)
            self.tagclick('span',kind)
            # 医生职称一级选择
            self.click(self.doctor_kind_select1)
            self.tagclick('span',doctor_kind1)
            # 医生职称二级选择
            self.click(self.doctor_kind_select2)
            self.tagclick('span','主治医师')
            time.sleep(1)
            # 擅长疾病输入框
            self.click(self.sick_text)
            self.input(self.sick_text,'新冠肺炎')
            # 提交保存按钮
            self.tagclick('span','保存')
        with allure.step('填写新增签约的内容'):
            # 工号填写
            self.click(self.text_id)
            self.input(self.text_id,idd)
            # 选择签约科室
            self.click(self.kinds_select)
            self.tagclick('span',kind)
        with allure.step('点击提交按钮'):
            self.tagclick('span','确定')
        with allure.step('审查实际结果'):
            time.sleep(3)
            text2 = self.getText(self.yeshu_text)
            time.sleep(1)
            assert text2 != text1
        self.quit_hospital()

    def delete_doctor(self,hospital_name):
        '''
        删除该医生信息的页面动作
        :return:
        '''
        with allure.step("初始化浏览器"):
            self.setup()
        with allure.step('选择相对应医院'):
            self.tagclick('li', hospital_name)
        with allure.step('切换至医生管理界面'):
            element2 = self.locator(self.doctor_card)
            ActionChains(self.driver).move_to_element(element2).click().perform()
            time.sleep(2)
            text1 = self.getText(self.yeshu_text)
            time.sleep(1)
        with allure.step('选择所要删除的医生'):
            element1 = self.locator(self.newdoctor_delete)
            ActionChains(self.driver).move_to_element(element1).click().perform()
            self.click(self.delete_button)
        with allure.step('审查是否删除'):
            time.sleep(2)
            text2 = self.getText(self.yeshu_text)
            time.sleep(1)
            assert text2 != text1
        with allure.step("退出浏览器"):
            self.quit_hospital()





