import time

import allure
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By

from base.selecet import select1


class doctor_manage_action(select1):
    '''
    机构管理模块的医生签约管理的页面动作
    '''
    # 新增医生的签约信息的服务配置按钮
    newdoctor_service_button = \
        '//div[@class="el-table__fixed-right"]/div[2]/table[1]/tbody[1]/tr[last()]/td[last()]/div[1]/span[2]'
    # 新增医生的编辑按钮
    newdoctor_edit_button =\
        '//div[@class="el-table__fixed-right"]/div[2]/table[1]/tbody[1]/tr[last()]/td[last()]/div[1]/span[1]'
    # 医师选项
    doctor_button1 = '//form[@class="el-form"]/div[1]/div[last()]/div[1]/div[1]/div[1]/label[1]/span[1]'
    # 医生签约管理url地址
    doctor_service_url = 'https://pre-release.jyhk.com/admin/#/organization/docManage'
    # 一键切换按钮
    switch_button = '//div[@class="el-dialog__wrapper z-dialog-drag-pub"]/div[1]/div[2]/div[1]/div[1]/span'
    # 保存配置按钮
    button1 = '//div[@class="el-dialog__wrapper z-dialog-drag-pub"]/div[1]/div[3]/div[1]/div[1]/button[2]'
    # 保存编辑按钮
    button2 = '//div[@class="el-dialog__wrapper z-dialog-pub"]/div[1]/div[3]/div[1]/div[1]/button[2]'
    # 删除按钮
    newdoctor_service_delete = '//*[@id="app"]/div/div[2]/section/div/div[2]/div[3]/div[5]/div[2]/table/tbody/tr[last()]/td[last()]/div[1]/span[3]'
    # 确定删除按钮
    delete_button = '//div[@class="el-message-box"]/div[3]/button[2]'
    # 信息条数显示框
    information_text = '//div[@id="app"]/div/div[2]/section/div/div[2]/div[4]/div[1]/span[1]'
    def setup(self):
        '''
        初始化浏览器
        :return:
        '''
        with allure.step("初始化浏览器"):
            self.jigou()
            self.driver.implicitly_wait(5)
            self.url(self.doctor_service_url)

    def quit_doctor(self):
        '''
        浏览器关闭
        :return:
        '''
        with allure.step("退出浏览器"):
            time.sleep(1)
            self.catch_png()
            time.sleep(2)
            self.quit()

    def doctor_service(self,hospital_name):
        '''
        医生服务配置的页面动作
        :return:
        '''
        self.setup()
        with allure.step("选择相对应的医院"):
            self.tagclick("li",hospital_name)
            time.sleep(2)
        with allure.step("点击新增医生的服务配置按钮"):
            element1 = self.locator(self.newdoctor_service_button)
            ActionChains(self.driver).move_to_element(element1).click().perform()
        with allure.step("点击一键切换按钮"):
            time.sleep(1)
            self.click(self.switch_button)
        with allure.step("点击保存按钮"):
            time.sleep(1)
            self.click(self.button1)
            time.sleep(1)
        with allure.step("点击医生的编辑按钮"):
            self.click(self.newdoctor_edit_button)
            time.sleep(1)
            self.click(self.doctor_button1)
            time.sleep(1)
            self.click(self.button2)
        self.quit_doctor()

    def doctor_delete(self,hospital_name):
        '''
        解除医生签约的页面动作
        :return:
        '''
        self.setup()
        with allure.step("选择相对应的医院"):
            self.tagclick("li", hospital_name)
        with allure.step("删除相对应的医生"):
            time.sleep(1)
            text1 = self.getText(self.information_text)
            time.sleep(3)
            element1 = self.locator(self.newdoctor_service_delete)
            ActionChains(self.driver).move_to_element(element1).click().perform()
        with allure.step('确定删除'):
            self.click(self.delete_button)
            time.sleep(1)
            text2 = self.getText(self.information_text)
            time.sleep(2)
            assert text2 != text1
        with allure.step("退出浏览器"):
            self.quit_doctor()







if __name__ == '__main__':
    doctor_manage_action().doctor_service()

