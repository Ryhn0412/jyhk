from tools.random_idcard import id_card
from tools.random_phone import get_phone_num
from tools.yaml_decode import get_yaml, yaml_locator


class host_url:
    '''
    用于一些辅助工具方法的数据生成
    '''

    def add_doctor_url(self):
        '''
        读取ymal地址
        :return:
        '''
        file1 = get_yaml(yaml_locator(r'\hospital_object\hospital_manage_object\add_doctor.ymal'))
        return file1
    def scheduling_url(self):
        '''
        读取ymal地址
        :return:
        '''
        file2 = get_yaml(yaml_locator(r'\hospital_object\schduling_object\scheduling_process.ymal'))
        return file2

    def order_select_url(self):
        '''
        读取ymal地址
        :return:
        '''
        file3 = get_yaml(yaml_locator(r'\shopping_object\order_object\order_select.ymal'))
        return file3

    def addimage_url(self):
        '''
        读取ymal地址
        :return:
        '''
        file4 = get_yaml(yaml_locator(r'\shopping_object\image_object\add_image.ymal'))
        return file4

    def selectimage_url(self):
        '''
        读取ymal地址
        :return:
        '''
        file5 = get_yaml(yaml_locator(r'\shopping_object\image_object\select_image.ymal'))
        return file5


    def idcard1(self):
        '''
        读取随机生成的身份证号
        :return:
        '''
        iddenfiy = id_card()
        return iddenfiy

    def ran_phone(self):
        '''
        读取随机生成的手机号
        :return:
        '''
        iphone = get_phone_num()
        return iphone