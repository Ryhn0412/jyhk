import os

import pytest

from tools.emailzip import make_zip

# 启动测试
pytest.main(['-s', '-v', '--alluredir', './Suites/report/result/'])
# 生成报告文件
os.system('allure generate ./Suites/report/result/ -o ./Suites/report/allure1 --clean')
# 获取报告路径
report_dir = os.getcwd() + "./Suites/report"
# 调用打包方法，对报告进行ZIP压缩
make_zip(report_dir, "test_report" + ".zip")
